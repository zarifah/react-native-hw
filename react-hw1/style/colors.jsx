const Colours = {
    main: '#FF7676',
    tranparentYellow: '#FFE194',
    Yellow: '#FFD976',
    dark: '#303234',
    light: '#EEEEEE'
};

export default Colours;