import close from "../assets/close_icon.png";
import edit from "../assets/edit.png";
import arrowBack from "../assets/arrowback.png";
import menu from "../assets/menu.png";
import save from "../assets/save.png";
import user from "../assets/user_icon.png";
import transparent from "../assets/transparent.png"
const images = {
  close,
  edit,
  arrowBack,
  menu,
  save,
  user,
  transparent
};

export default images;
