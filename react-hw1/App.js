import React,{useState} from "react";
import { AppLoading } from 'expo';
import { RootNav } from "./navigation";
import { Fonts } from './style/fonts';
import { Provider } from "react-redux";
import store from "./redux";

export default function App() {
  const [loaded, setLoaded] = useState(false)
  
  if (!loaded) {
    return (
      <AppLoading
        startAsync={Fonts}
        onFinish={() => {setLoaded(true)}}
        onError={() => {console.log('There is some problem in  Fonts')}}
      />
    )
  }
  return (
    <Provider store={store}>
      <RootNav/>
    </Provider>
  );
}
