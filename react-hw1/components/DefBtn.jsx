import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet } from 'react-native'
import { DefText} from './DefText';
import Colours from '../style/colors'

export class DefBtn extends Component {
    render() {

        const {style, onPressHandler, text, textStyle} = this.props;
        return (
            <TouchableOpacity
                style={[styles.btnContainer, style]}
                onPress={() => { onPressHandler() }}>
                <DefText
                    weight="bold"
                    style={[styles.btnText, textStyle]}>{ text }</DefText>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    btnContainer: {
        alignSelf: 'center',
        width: '85%',
        height: 45,
        backgroundColor: Colours.main,
        borderRadius: 45,
        alignItems: 'center',
        justifyContent: 'center'

    },
    btnText: {
        textTransform: 'uppercase',
        color: 'white'
    }
})