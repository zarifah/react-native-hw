import React from 'react';
import { Text } from "react-native";

const fonts = {
    regular: 'MontserratRegular',
    medium: 'MontserratMedium',
    bold: 'MontserratBold'
}
export const DefText = ({children, weight, style, ...rest}) => {
  return <Text {...rest} 
  style={[{ fontFamily: fonts[weight] || fonts.regular }, style]}>{children}</Text>
}