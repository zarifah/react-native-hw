import React from 'react';
import {DefText} from './DefText';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';

import { getData } from '../redux/data';
import Colours from '../style/colors';
import images from '../style/images';


const mapStateToProps = state => ({
    data: getData(state)
  })

  
export const EditItemCard = connect(mapStateToProps)(({data, listType, listID, itemID, editHandler, deleteHandler, details, isEdit, editItemID}) => {
    
    const { itemName = '', count = '0', isBought = false, type = '' } = details?.find(item => item.id === itemID);
    return (
        <View style={styles.container}>
            <TouchableOpacity disabled={editItemID === itemID ? true : false} style={[styles.editBtn, {opacity: editItemID === itemID ? 0.5 : 1}]} onPress={() => {editHandler(itemName, count, type, itemID)}}>
                <Image source={images.edit} />
            </TouchableOpacity>
            <View style={styles.itemList}>
                <DefText weight="medium">{itemName}</DefText>
                <DefText weight="medium">x{count} {type}</DefText>

            </View>
            <TouchableOpacity style={styles.closeBtn} onPress={() => {deleteHandler(itemID)}}>
                <Image source={images.close} />
            </TouchableOpacity>
        </View>
    )
})

const styles = StyleSheet.create({
    container: {
        width: '90%',
        height: 45,
        borderRadius: 45,
        borderColor: Colours.Yellow,
        borderWidth: 3,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    editBtn: {
        backgroundColor: Colours.Yellow,
        height: '100%',
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 45,
        width: 40,
        justifyContent: 'center'
        
    },
    closeBtn: {
        backgroundColor: Colours.main,
        height: '100%',
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 45,
        width: 40,
        justifyContent: 'center'
    },
    itemList: {
        flexDirection: 'row',
        width: '65%',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
})