import React from 'react';
import {DefText} from './DefText';
import { View, StyleSheet, TouchableOpacity } from 'react-native';

import Colours from '../style/colors';

export const ListItemCard = ({ item: {id, name, isCompleted, details }, onPressHandler, onLongPressHandler, type}) => {

    const maxCount = details?.length;
    const boughtItemsCount = details?.filter(v => v.isBought).length
    const percent = boughtItemsCount / maxCount * 100 || 0;

    if(type == 'oneTimeList') {
        isCompleted = boughtItemsCount === maxCount;
    }
  return (
    <View style={styles.container}>
        <TouchableOpacity
         style={[styles.card,{ borderColor: isCompleted ? Colours.tranparentYellow : Colours.Yellow}]}
         onPress={onPressHandler}
         onLongPress={onLongPressHandler}>
            <View style={styles.header}>
                <DefText
                 weight="bold" 
                 style={[styles.title, {color: isCompleted ? Colours.dark : 'black'}]}>{name}</DefText>
                <DefText
                 weight="medium" 
                 style={[styles.details, {color: isCompleted ? Colours.dark : 'black'}]}>{boughtItemsCount} / {maxCount}</DefText>
            </View>
            <View style={styles.itemList}>
                <View style={styles.item}>
                    <View style={[styles.itemSale, {width: `${percent}%`, backgroundColor: isCompleted ? Colours.tranparentYellow : Colours.Yellow }]}>

                    </View>
                </View>
            </View>
        </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center'
    },
    card: {
        height: 100,
        width: '90%',
        borderRadius: 10,
        borderWidth: 3,
        padding: 15
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    title: {
        fontSize: 18,
    },
    details: {
        fontSize: 14,
        color: Colours.dark
    },
    itemList: {
        alignItems: 'center'
    },
    item: {
        width: '100%',
        height: 25,
        marginTop: 15,
        borderRadius: 20,
        backgroundColor: Colours.light,
        overflow: 'hidden'
    },
    itemSale: {
        height: '100%',
        borderRadius: 20
    }
})