import React from 'react';
import {DefText} from './DefText';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

import { getData } from '../redux/data';

import Colours from '../style/colors';

const mapStateToProps = state => ({
    data: getData(state)
  })

export const ItemCard = connect(mapStateToProps)(({ listType, data, item: { id }, listId, onLongPressHandler, onPressHandler }) => {

    const { itemName, count, isBought, type } = data[listType].find(list => list.id === listId)?.details?.find(item => item.id === id);
    return (
        <View style={styles.container}>
            <TouchableOpacity
                style={[styles.card, isBought ? styles.bought : {}]}
                onPress={onPressHandler}
                onLongPress={onLongPressHandler}
                disabled={isBought}>
                <DefText weight={isBought ? 'regular' : 'medium'} style={isBought ? styles.boughtText : styles.title}>{itemName}</DefText>
                <DefText style={isBought ? styles.boughtText : styles.title}>x{count} {type}</DefText>
            </TouchableOpacity>
        </View>
    )
})

const styles = StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center'
    },
    card: {
        width: '87%',
        alignItems: 'center',
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderRadius: 27,
        height: 45,
        borderColor: Colours.Yellow,
        borderWidth: 3
    },
    bought: {
        borderColor: Colours.tranparentYellow
    },
    boughtText: {
        color: Colours.dark,
        opacity: .5
    },
    title: {
        color: Colours.dark,
        opacity: 1
    }
})