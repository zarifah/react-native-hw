import React, { useState, useEffect } from 'react'
import {
  View,
  StyleSheet,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  Alert,
  AsyncStorage
} from 'react-native';
import { Layout } from '../commons';
import { connect } from 'react-redux';
import {
  getData,
  addUserDetal
} from '../redux/data';
import {
  DefText,
  DefBtn
} from '../components'




import Colours from '../style/colors';

const mapStateToProps = (state) => ({
  data: getData(state)
});


export const UserSettings = connect(mapStateToProps, { addUserDetal })
(({ navigation, data, addUserDetal }) => {

  const [fields, setFields] = useState({
    username: data.username,
    imgUrl: data.imgUrl
  })
  const [accessGranted, setAccessGranted] = useState(false);

  const fieldsHandler = (name, value) => {
    setFields(v => ({
      ...v,
      [name]: value
    }))
  }

  const updateAsyncStorage = async (userData) => {
    try {
      const newData = {
        ...data,
        username: userData.username,
        imgUrl: userData.imgUrl
    };

      await AsyncStorage.setItem('data', JSON.stringify(newData));

    } catch (e) {
      console.log('Add list Screen', e);
    }
  }

  const saveHandler = () => {
    if (fields.username.trim()) {
      const userData = {
        username: fields.username.trim(),
        imgUrl: fields.imgUrl
      }



      addUserDetal(userData);
      updateAsyncStorage(userData);
      Keyboard.dismiss();
      navigation.navigate('One Time Lists')
    } else {
      Alert.alert('Warning', 'Username already exists')
    }

  }


  return (
    <Layout header="User Settings" >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <View style={styles.inputContainer}>
            <DefText weight='medium' style={styles.label}>username</DefText>
            <TextInput
              onChangeText={(v) => { fieldsHandler('username', v) }}
              style={styles.field}
              placeholder="username" 
              value={fields.username}
              />
          </View>

          <View style={styles.inputContainer}>
            <DefText weight='medium' style={styles.label}>avatar url</DefText>
            <TextInput
              onChangeText={(v) => { fieldsHandler('imgUrl', v) }}
              style={styles.field}
              placeholder="image url"
              value={fields.imgUrl}
              />
              
          </View>



          

          <DefBtn
            onPressHandler={saveHandler}
            text="Save changes"
            style={{ marginTop: 25 }}
          />

        </View>
      </TouchableWithoutFeedback>
    </Layout>

  )
})

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  field: {
    width: '85%',
    height: 45,
    backgroundColor: Colours.light,
    color: Colours.dark,
    borderRadius: 45,
    fontSize: 20,
    paddingLeft: 13,
    fontFamily: 'MontserratMedium'
  },
  label: {
    marginBottom: 7,
    color: Colours.dark
  },
  inputContainer: {
    width: '100%',
    alignItems: 'center',
    marginTop: 23,

  },

})