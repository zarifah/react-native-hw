import React, { useState, Component,ComponentClass} from 'react'
import {
  View,
  StyleSheet, Keyboard,
  TouchableWithoutFeedback,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
  AsyncStorage
} from 'react-native';
import { connect } from 'react-redux'
import uuid from 'react-uuid'


import { Layout } from '../commons';
import {DefText, DefBtn as DefBtn, EditItemCard } from '../components'
import { getData, updateItem } from '../redux/data';
import Colours from '../style/colors';
import { FlatList } from 'react-native-gesture-handler';


const mapStateToProps = (state) => ({
  data: getData(state)
});


export const EditItem = connect(mapStateToProps, {
  updateItem
})(({
  navigation,
  data,
  updateItem,
  route: { params: { id, type } } }) => {

  const { details, name } = data[type].find(list => list.id === id)



  const [fields, setFields] = useState({
    itemName: '',
    count: '0',
    unit: 'pkg',
    isEdit: false,
    editItemID: ''
  })

  const [detailsState, setDetailsState] = useState(details)



  const updateAsyncStorage = async (saveItem) => {
    try {
      const newData = {
        ...data,
        [saveItem.type]: data[saveItem.type].map(list => {
            if(list.id === saveItem.listID) {
                return {
                    ...list,
                    details: saveItem.details
                }
            }
            return list
        })
    };

      await AsyncStorage.setItem('data', JSON.stringify(newData));

    } catch (e) {

      console.log('Already save all changes', e);
    }
  }

  const saveBtnHandler = () => {

    const saveItem = {
      type,
      listID: id,
      details: detailsState
    }
    updateItem(saveItem)
    updateAsyncStorage(saveItem)
    navigation.goBack();
  }


  const fieldsHandler = (name, value) => {
    setFields(v => ({
      ...v,
      [name]: value
    }))
  }
  const countNumHandler = (v) => {
    console.log(v);

    if (!isNaN(+v)) {
      fieldsHandler('count', v);
    }

    if (v === '') {
      fieldsHandler('count', '0');

    }
  }
  const increaseHandler = () => {
    setFields(v => ({
      ...v,
      count: (+v.count + 1).toString()
    }))
    
  }
  const decreaseHandler = () => {
    setFields(v => {
      if(v.count == '0') {
        return v;
      }
      return {
      ...v,
      count: (+v.count - 1).toString()
    }}
    
    
    )
  }
  const editItemHandler = (newItemName, newCount, newType, itemID) => {
    setFields(v => ({
      ...v,
      isEdit: true,
      count: newCount.toString(),
      itemName: newItemName,
      unit: newType,
      editItemID: itemID
    }))
  }
  const deleteItemHandler = (itemID) => {
 
    setDetailsState(v => v.filter(item => item.id !== itemID))
  }
  const onAddHandler = () => {
    if(!fields.itemName) {
      Alert.alert('Warning', 'This product name already exists ')
      return;
    }
    if(fields.count == '0') {
      Alert.alert('Warning', 'Count cannot be 0')
      return;
    }


    setDetailsState(v => [
      ...v,
      {
        id: uuid(),
        itemName: fields.itemName,
        count: fields.count,
        type: fields.unit,
        isBought: false
    }
    ])

    setFields({
      itemName: '',
      count: '0',
      unit: 'pkg',
      isEdit: false
    })
  }
  const onCancelHandler = () => {
    setFields({
      itemName: '',
      count: '0',
      unit: 'pkg',
      isEdit: false,
      editItemID: ''
    })
  }
  const onUpdateHandler = () => {
    setDetailsState(v => v.map(item => {
      if(item.id === fields.editItemID){
        return {
          ...item,
          itemName: fields.itemName,
          count: fields.count,
          type: fields.unit,
        }
      }
      return item
    }))

    setFields({
      itemName: '',
      count: '0',
      unit: 'pkg',
      isEdit: false,
      editItemID: ''
    })
  }
 



  return (
    <Layout back={true} header={name} save={saveBtnHandler}>
      
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        
          <View style={styles.container}>
            <View style={styles.header}>
              <View style={styles.inputSection}>
                <View style={styles.nameInput}>
                  <DefText style={styles.label}> position name</DefText>
                  <TextInput
                    value={fields.itemName}
                    style={styles.nameField}
                    placeholder="Enter product name"
                    onChangeText={(v) => { fieldsHandler('itemName', v) }}></TextInput>
                </View>

                <View style={styles.countInput}>
                  <DefText style={styles.label}>count</DefText>
                  <View style={styles.countNum}>
                    <TouchableOpacity
                     style={[styles.countBtn, { paddingLeft: 5 }]}
                     onPress={decreaseHandler}>
                      <DefText weight="bold" style={{ fontSize: 20 }}>-</DefText>
                    </TouchableOpacity>
                    <TextInput value={fields.count}
                      value={fields.count}
                      keyboardType="number-pad"
                      style={styles.countField}
                      onChangeText={(v) => { countNumHandler(v) }} />
                    <TouchableOpacity 
                    style={[styles.countBtn, { paddingRight: 5 }]}
                    onPress={increaseHandler}>
                      <DefText weight="bold" style={{ fontSize: 20 }}>+</DefText>
                    </TouchableOpacity>

                  </View>
                </View>
              </View>





              <View style={styles.btnSection}>
                <TouchableOpacity
                  style={[styles.radioBtn, { backgroundColor: fields.unit === 'pkg' ? '#FADBD8' : "#AF7AC5" }]}
                  onPress={() => { fieldsHandler('unit', 'pkg') }}>
                  <DefText
                    weight={fields.unit === 'pkg' ? 'bold' : ''}
                    style={styles.radioText}>pkg</DefText>
                </TouchableOpacity>

                <TouchableOpacity
                  style={[styles.radioBtn, { backgroundColor: fields.unit === 'kg' ? '#FADBD8' : "#AF7AC5" }]}
                  onPress={() => { fieldsHandler('unit', 'kg') }}>
                  <DefText
                    weight={fields.unit === 'kg' ? 'bold' : ''}
                    style={styles.radioText}>kg</DefText>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.radioBtn, { backgroundColor: fields.unit === 'litre' ? '#FADBD8' : "#AF7AC5" }]}
                  onPress={() => { fieldsHandler('unit', 'litre') }}>
                  <DefText
                    weight={fields.unit === 'litre' ? 'bold' : ''}
                    style={styles.radioText}>litre</DefText>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.radioBtn, { backgroundColor: fields.unit === 'bott' ? '#FADBD8' : "#AF7AC5" }]}
                  onPress={() => { fieldsHandler('unit', 'bott') }}>
                  <DefText
                    weight={fields.unit === 'bott' ? 'bold' : ''}
                    style={styles.radioText}>bott</DefText>
                </TouchableOpacity>
            </View>



            {
              !fields.isEdit ?
                <DefBtn
                  text="Add to list"
                  style={[styles.addBtn, { width: '90%', marginTop: 25 }]}
                  onPressHandler={onAddHandler}
                />
                :
                <View style={styles.isEditModeBtn}>
                  <DefBtn
                    text="cancel"
                    style={[styles.addBtn, { width: '40%', marginTop: 25, opacity: 0.7 }]}
                    onPressHandler={onCancelHandler}
                  />
                  <DefBtn
                    text="update"
                    style={[styles.addBtn, { width: '40%', marginTop: 25 }]}
                    onPressHandler={onUpdateHandler}
                  />
                </View>
            }

          </View>



          <View style={styles.separator} />


          <View style={styles.body}>
            <FlatList
              data={detailsState}
              renderItem={({ item }) => (
                <View style={styles.itemDetailWrapper}>
                  <EditItemCard
                    listType={type}
                    listID={id}
                    itemID={item.id}
                    editHandler={editItemHandler}
                    deleteHandler={deleteItemHandler}
                    details={detailsState}
                    isEdit={fields.isEdit}
                    editItemID={fields.editItemID}
                  />
                </View>)}
            />
          </View>

        </View>
      </TouchableWithoutFeedback>

    </Layout>
  )
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    width: '100%',
    paddingTop: 20,
  },
  body: {
     flex: 1
  },
  inputSection: {
    flexDirection: "row",
    alignItems: 'center',
    paddingHorizontal: 5
  },
  nameInput: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center'
  },
  countInput: {
    flex: 2,
    alignItems: 'center'
  },
  nameField: {
    width: '92%',
    height: 45,
    backgroundColor: Colours.light,
    color: Colours.dark,
    fontFamily: 'MontserratBold',
    borderRadius: 45,
    paddingHorizontal: 13,
    fontSize: 14,
   
    
  },
  countField: {
    flex: 2,
    fontFamily: 'MontserratBold',
    textAlign: 'center'
  },
  label: {
    marginBottom: 7,
    color: Colours.dark
  },

  countNum: {
    flexDirection: 'row',
    backgroundColor: Colours.light,
    borderRadius: 45,
    height: 45,
    width: '70%',
    alignItems: 'center',
    overflow: 'hidden'
  },

  countBtn: {
    flex: 2,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnSection: {
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%'
  },

  radioBtn: {
    backgroundColor: Colours.light,
    height: 45,
    alignItems: 'center',
    width: '22%',
    justifyContent: 'center',
    borderRadius: 38,
    overflow: 'hidden'
  },
  radioText: {
    fontSize: 16
  },
  separator: {
    marginVertical: 25,
    width: '100%',
    backgroundColor: Colours.light,
    height: 3,
    position: 'relative'
  },
  itemDetailWrapper: {
    alignItems: 'center',
    marginVertical: 5
  },
  isEditModeBtn: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  }
})