import React, { useState, useEffect } from 'react'
import { View, StyleSheet, Alert, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native'

import { Layout } from '../commons';
import { getData, deleteList, setAllData, items} from '../redux/data';
import { ListItemCard } from '../components/ListItemCard';
import { FlatList } from 'react-native-gesture-handler';


const mapStateToProps = state => ({
  data: getData(state)
})

export const OneTime = connect(mapStateToProps, { deleteList, setAllData })(({data, setAllData, deleteList, data: { oneTimeList} }) => {
  const navigation = useNavigation();
  const [loaded, setLoaded] = useState(false)

  const onLongPressHandler = (id) => {
    Alert.alert("Are you sure  really want to delete? If yes click delete", '', [
      {
        text: 'Cancel', style: 'cancel'
      },
      {
        text: 'Delete', onPress: () => {
          const deletedList = {
            type: 'oneTimeList',
            listID: id,
            items
          }

          updateAsyncStorage(deletedList)
          deleteList(deletedList)
          
        }
      }
    ])
  }
  
  const updateAsyncStorage = async (deletedList) => {
    try {
      const newData = {
        ...data,
        [deletedList.type]: data[deletedList.type].filter(list => list.id !== deletedList.listID)
      };

      await AsyncStorage.setItem('data', JSON.stringify(newData));

    } catch (e) {
      console.log('Ok, remove this list on One Time List ', e);
    }
  }

  
  useEffect(() => {
    (async () => {
      try {
        const value = await AsyncStorage.getItem('data').then(v => JSON.parse(v));
        if(value !== null) {
          setAllData({
            data: value
          })
        }
      } catch (e) {
        console.log('One Time List ', e);
      }
    })()
  }, [])

  return (
    <Layout header="One Time Lists" drawer={true} >
      <View style={styles.container}>
        <FlatList data={oneTimeList}
          renderItem={({ item }) => (
            <View style={styles.card}>
              <ListItemCard
                item={item}
                onLongPressHandler={() => { 
                  const { id } = item;
                  onLongPressHandler(id)
                 }}
                 type='oneTimeList'
                onPressHandler={() => { navigation.navigate('One List', { item,  type: 'oneTimeList',items })}} />
            </View>
          )}/>
      </View>
    </Layout>
  )
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 25
  },
  card: {
    marginBottom: 15,
  }
})
