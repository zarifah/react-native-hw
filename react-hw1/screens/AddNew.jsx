import React, { useState,Component } from 'react'
import {
  View,
  StyleSheet, Keyboard,
  TouchableWithoutFeedback,
  TextInput,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from 'react-native';
import { connect } from 'react-redux'
import uuid from 'react-uuid'

import { Layout } from '../commons';
import { DefText, DefBtn} from '../components'
import { getData, createList } from '../redux/data';

import Colours from '../style/colors';


const mapStateToProps = (state) => ({
  data: getData(state)
});

export const AddNew = connect(mapStateToProps, { createList })(({ data, createList, navigation }) => {

  const [fields, setFields] = useState({
    listName: '',
    listType: 'oneTimeList'
  })

  const fieldsHandler = (name, value) => {
    setFields(v => ({
      ...v,
      [name]: value
    }))
  }


  const createListHandler = () => {
    if(fields.listName.trim()) {

      const newList = {
        id: uuid(),
        type: fields.listType,
        name: fields.listName.trim(),
        isCompleted: false,
        details: []
      }
      createList(newList)
      updateAsyncStorage(newList)


      setFields(v => ({
        listName: '',
        listType: 'oneTimeList'
      }))
      navigation.navigate(fields.listType === 'oneTimeList' ? 'One Time List' : 'Regular Lists')
    } else {
      Alert.alert('Warning', 'List name already exists')
    }
  }
  
  const updateAsyncStorage = async (newList) => {
    try {
      const newData = {
        ...data,
        [newList.type]: [
          ...data[newList.type],
          newList
        ]
      };

      await AsyncStorage.setItem('data', JSON.stringify(newData));

    } catch (e) {
      console.log('Added new list', e);
    }
  }

  return (
    <Layout header="Add New Lists">
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <View style={styles.inputContainer}>
            <DefText weight='medium' style={styles.label}>list name</DefText>
            <TextInput
              onChangeText={(v) => { fieldsHandler('listName', v) }}
              style={styles.field}
              placeholder="Add list name"
              value={fields.listName}
            />
          </View>



          <View style={styles.radioContainer}>
            
            <TouchableOpacity
              style={[styles.radioBtn, {backgroundColor: fields.listType === 'oneTimeList' ? '#FADBD8' : "#AF7AC5"}]}
              onPress={() => {fieldsHandler('listType', 'oneTimeList')}}>
              <DefText
                weight={fields.listType === 'oneTimeList' ? 'bold' : ''}
                style={styles.radioText}>One Time List</DefText>
            </TouchableOpacity>
            
            <TouchableOpacity
              style={[styles.radioBtn, {backgroundColor: fields.listType === 'regularList' ? '#FADBD8' : "#AF7AC5"}]}
              onPress={() => {fieldsHandler('listType', 'regularList')}}>
              <DefText
                weight={fields.listType === 'regularList' ? 'bold' : ''}
                style={styles.radioText}>Regular List</DefText>
            </TouchableOpacity>
          </View>



          <DefBtn
            text="create list"
            style={{ marginTop: 25 }}
            onPressHandler={createListHandler}
          />
        </View>
      </TouchableWithoutFeedback>
    </Layout>
  )
})

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  field: {
    
    paddingLeft: 13,
    
    width: '85%',
    height: 45,
    backgroundColor: Colours.light,
    borderRadius: 45,
    fontSize: 20,
    color: Colours.dark,
    fontFamily: 'MontserratMedium'
  },
  label: {
    marginBottom: 5,
    color: Colours.dark
  },
  inputContainer: {
    width: '100%',
    alignItems: 'center',
    marginTop: 23,

  },
  radioContainer: {
    width: '85%',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 25,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  radioBtn: {
    backgroundColor: Colours.light,
    height: 45,
    alignItems: 'center',
    width: '47%',
    justifyContent: 'center',
    borderRadius: 38,
    overflow: 'hidden'
  },
  radioText: {
    fontSize: 16
  }
})