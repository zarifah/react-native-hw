import React, { useEffect } from 'react'
import { View, StyleSheet, FlatList, Alert, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { getData, resetItems, toggleitem } from '../redux/data';

import { Layout } from '../commons';
import { DefText, DefBtn, ItemCard } from '../components'

const mapStateToProps = state => ({
  data: getData(state)
})




export const OneList = connect(mapStateToProps, {
  resetItems,
  toggleitem,
})(({ navigation,
  resetItems,
  toggleitem,
  data,
  route: {
    params: {
      item: { id }, type
    }
  } }) => {


  const { details, name } = data[type].find(list => list.id === id)
  const maxCount = details?.length;
  const boughtItemsCount = details?.filter(v => v.isBought).length


  const onPressHandler = (itemID) => {
    Alert.alert("If  you are sure really want to buy this product, click buy", '', [
      {
        text: 'Cancel', style: 'cancel'
      },
      {
        text: 'Buy', onPress: () => {

          const toggleItem = {
            type,
            listID: id,
            itemID: itemID
          }

          updateAsyncStorageToggle(toggleItem)

          toggleitem(toggleItem)
        }
      }
    ])
  }

  const onEditHandler = () => {
    navigation.navigate('Edit Item',{ id, type })
  }

  const updateAsyncStorage = async (resetList) => {
    try {
      const newData = {
        ...data,
        [resetList.type]: data[resetList.type].map(list => {
            
            if(list.id === resetList.listID) {
                return {
                    ...list,
                    details: list.details.map(item => {
                        if(item.isBought) {
                            return {
                                ...item,
                                isBought: false
                            }
                        }
                        return item
                    })
                }
            }
            return list
        })
    };

      await AsyncStorage.setItem('data', JSON.stringify(newData));

    } catch (e) {

      console.log('Your wish is accepted. Reset all items on One List Screen', e);
    }
  }

  const updateAsyncStorageToggle = async (toggleItem) => {
    try {
      const newData = {
        ...data,
        [toggleItem.type]: data[toggleItem.type].map(list => {
            if(list.id === toggleItem.listID) {
                return {
                    ...list,
                    details: list.details.map(item => {
                        if(item.id === toggleItem.itemID) {
                            return {
                                ...item,
                                isBought: !item.isBought
                            }
                        }
                        return item
                    })
                }
            }
            return list
        })
    };

      await AsyncStorage.setItem('data', JSON.stringify(newData));

    } catch (e) {

      console.log('Toggle item on OneListScreen', e);
    }
  }
  const resetBtnHandler = () => {
    Alert.alert("Are you sure  really want to reset all items?", '', [
      {
        text: 'Cancel', style: 'cancel'
      },
      {
        text: 'Reset', onPress: () => {
          const resetList = {
            type,
            listID: id
          };

          updateAsyncStorage(resetList);
          resetItems(resetList)
        
        }
      }
    ])
  }

  return (
    <Layout header={name} back={true} edit={onEditHandler}>
      <View style={styles.container}>
        <View style={[styles.body, {justifyContent: type === 'regularList' ? 'flex-end' : 'space-between'}]}>
          <DefBtn
            text="reset"
            style={[{ height: 22, width: 80 }, type === 'regularList' ? styles.displayNone : {}]}
            textStyle={{ fontSize: 11 }}
            onPressHandler={resetBtnHandler}
          />

          <DefText weight="medium">{boughtItemsCount} / {maxCount}</DefText>
        </View>

        <FlatList data={details}
          renderItem={({ item }) => (
            <View style={styles.card}>
              <ItemCard
                listId={id}
                listType={type}
                item={item}
                onPressHandler={() => {
                  const { id } = item
                  onPressHandler(id)

                }} />
            </View>
          )} />


      </View>
    </Layout>
  )
})





const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body: {
    width: '87%',
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    marginBottom: 15
  },
  displayNone: {
    display: 'none'
  },


  card: {
    marginBottom: 15,
  }
})