import React, { Component } from 'react';
import { View, StyleSheet, StatusBar, Image, TouchableOpacity } from 'react-native';
import { useSafeArea } from 'react-native-safe-area-context'
import { useNavigation } from '@react-navigation/native';


import Colours from '../style/colors';
import {DefText} from '../components/DefText'

import images from '../style/images'
export const Layout = ({ children, header = 'undefined', back = false, drawer = false, edit = false, save = false, }) => {
    const insets = useSafeArea();
    const navigation = useNavigation();
    const screen = back || drawer || edit || save;
    return (
        <View style={[styles.container, {
            paddingTop: insets.top,
            paddingLeft: insets.left,
            paddingBottom: insets.bottom,
            paddingRight: insets.right,
        }]}>
            <StatusBar barStyle='light-content' />


            {screen &&
                <View style={styles.screenView} >
                    <TouchableOpacity disabled={!back} onPress={navigation.goBack}>
                        <Image style={back ? {} : styles.transparentStyle} source={back ? images.arrowBack : images.transparent} />
                    </TouchableOpacity>
                    <DefText style={styles.screenText} weight='medium'>{header}</DefText>
                    <TouchableOpacity onPress={
                        drawer ? navigation.toggleDrawer :
                            save ? save :
                                edit ? edit :
                                    () => {}}>
                        <Image style={drawer || edit || save ? {} : styles.transparentStyle} source={
                            drawer ? images.menu :
                                save ? images.save :
                                    edit ? images.edit :
                                        images.transparent
                        } />
                    </TouchableOpacity>
                </View>}

            {
                !back && !drawer && !edit && !save &&
                <View style={styles.header}>
                    <DefText weight='medium' style={styles.headerText}>{header}</DefText>
                </View>
            }



            <View style={styles.body}>
                {children}
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colours.main
    },
    body: {
        flex: 1,
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        backgroundColor: 'white',
        overflow: 'hidden'
    },
    header: {
        height: 75,
        justifyContent: 'center'
    },
    headerText: {
        color: 'white',
        textAlign: 'center',
        fontSize: 20
    },
    screenView: {
        height: 75,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 12
    },
    screenText: {
        color: 'white',
        textAlign: 'center',
        fontSize: 20
    },
    transparentStyle: {
        width: 15,
        height: 5
    }

})