import React from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { useSafeArea } from 'react-native-safe-area-context';
import { DefText} from '../components/DefText'
import images from '../style/images';
import Colours from '../style/colors';
import { getData } from '../redux/data';
import { connect } from 'react-redux';



const mapStateToProps = (state) => ({
    data: getData(state)
  });

export const Drawer = connect(mapStateToProps)(({ navigation, data: { username, imgUrl } }) => {
    const insets = useSafeArea();
    return (
        <View style={[styles.container, {
            paddingTop: insets.top,
            paddingLeft: insets.left,
            paddingBottom: insets.bottom,
            paddingRight: insets.right,
        }]}>
            <View style={styles.header}>
                <Image style={styles.profileImg} source={imgUrl ? {uri: imgUrl} : images.user } />
                <DefText style={styles.headerText}>{ username }</DefText>
            </View>

            <View style={styles.contents}>
                <TouchableOpacity style={[styles.drawerBtn, {marginBottom: 35}]} onPress={() => navigation.navigate('Add New List')}>
                    <DefText weight="bold" style={styles.listName}> Add new list </DefText>
                </TouchableOpacity>
                <TouchableOpacity style={styles.drawerBtn} onPress={() => navigation.navigate('One Time Lists')}>
                    <DefText weight="bold" style={styles.listName}> One time list </DefText>
                </TouchableOpacity>
                <TouchableOpacity style={styles.drawerBtn} onPress={() => navigation.navigate('Regular Lists')}>
                    <DefText weight="bold" style={styles.listName}> Regular list </DefText>
                </TouchableOpacity>
                <TouchableOpacity style={styles.drawerBtn} onPress={() => navigation.navigate('User Settings')}>
                    <DefText weight="bold" style={styles.listName}> User settings </DefText>
                </TouchableOpacity>
            </View>
        </View>
    )
})


const styles = StyleSheet.create({
    container: {
        flex: 1,
        overflow: 'hidden'
    },
    profileImg: {
        width: 60,
        height: 60,
        borderRadius: 100,
        
        borderWidth: 5,
        borderColor: Colours.main,
        overflow: 'hidden',
        marginLeft: 15
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15,
        marginTop: 15,
    },
    headerText: {
        fontSize: 24,
        marginLeft: 15,
        color: Colours.dark,
        opacity: 0.65
    },
    contents: {
        paddingTop: 15,
        backgroundColor: Colours.main,
        flex: 1,
        borderTopStartRadius: 25,
        borderTopRightRadius: 25,
        alignItems: 'center'
    },
    drawerBtn: {
        backgroundColor: Colours.light,
        width: 250,
        height: 34,
        marginBottom: 15,
        borderRadius: 38,
        justifyContent: 'center'

    },
    listName: {
        textTransform: 'uppercase',
        color: Colours.main,
        textAlign: 'center',
    }
})