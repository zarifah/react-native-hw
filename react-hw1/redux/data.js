import uuid from 'react-uuid';
// ACTION TYPES
const ADD_USER_DETAL = 'ADD_USER_DETAL';
const CREATE_LIST = 'CREATE_LIST';
const DELETE_LIST = 'DELETE_LIST';

const RESET_items = 'RESET_items';
const UPDATE_ITEM = 'UPDATE_ITEM';
const TOGGLE_items = 'TOGGLE_items';
const SET_ALL_DATA = 'SET_ALL_DATA';

// SELECTORS

const MODULE_NAME = "data";
export const getData = (state) => state[MODULE_NAME].data;

// REDUCER

const initialState = {
    data: {
        id: uuid(),
        imgUrl: 'https://ps.w.org/ultimate-member/assets/icon-256x256.png?rev=2143339',
        username: 'Username',

        oneTimeList: [{
                id: uuid(),
                name: "Everything for breakfast",
                type:'oneTimeList',
                items: [{
                        id: uuid(),
                        name: "Pasta",
                        isEdit: true,
                        count: 2,
                        unit: "pkg"


                    },
                    {
                        id: uuid(),
                        name: "Salt",
                        isEdit: true,
                        count: 1,
                        unit: "pkg"
                    },
                    {
                        id: uuid(),
                        name: "Tomatoes",
                        isEdit: false,
                        count: 1,
                        unit: "kg"
                    },
                    {
                        id: uuid(),
                        name: "Milk",
                        isEdit: false,
                        count: 0.3,
                        unit: "litre"
                    },

                ],
            },
            {
                id: uuid(),
                name: "Evening with pasta",
                type:'oneTimeList',
                items: [{
                        id: uuid(),
                        name: "Spaghetti",
                        isEdit: true,
                        count: 1,
                        unit: "pkg"
                    }, {
                        id: uuid(),
                        name: "Vermicelli",
                        isEdit: false,
                        count: 0.5,
                        unit: "pkg"

                    }, {
                        id: uuid(),
                        name: "Strozzapreti",
                        isEdit: true,
                        count: 1.5,
                        unit: "pkg"

                    },
                    {
                        id: uuid(),
                        name: "Fettuccine",
                        isEdit: true,
                        count: 2,
                        unit: "pkg"
                    },
                ],
            },
            {
                id: uuid(),
                name: "Kitchen Repair",
                type:"oneTimeList",
                items: [{
                        id: uuid(),
                        name: "Oven",
                        isEdit: false,
                        count: 2,
                        unit: "pkg"
                    }, {
                        id: uuid(),
                        name: "Cooler",
                        isEdit: true,
                        count: 1,
                        unit: "pkg"

                    }, {
                        id: uuid(),
                        name: "Microwave",
                        isEdit: false,
                        count: 1,
                        unit: "pkg"

                    },
                    {
                        id: uuid(),
                        name: "Mixer",
                        isEdit: false,
                        count: 2,
                        unit: "pkg"
                    },
                    {
                        id: uuid(),
                        name: "Toaster",
                        isEdit: true,
                        count: 1,
                        unit: "pkg"
                    },
                ],
            },
        ],
        regularList: [{
                id: uuid(),
                name: "Everything for breakfast",
                type:'regularList',
                items: [{
                        id: uuid(),
                        name: "Pasta",
                        isEdit: true,
                        count: 2,
                        unit: "pkg"


                    },
                    {
                        id: uuid(),
                        name: "Salt",
                        isEdit: false,
                        count: 1,
                        unit: "pkg"
                    },
                    {
                        id: uuid(),
                        name: "Tomatoes",
                        isEdit: true,
                        count: 1,
                        unit: "kg"
                    },
                    {
                        id: uuid(),
                        name: "Milk",
                        isEdit: true,
                        count: 0.3,
                        unit: "litre"
                    },

                ],
            },
            {
                id: uuid(),
                name: "Evening with pasta",
                type:'regularList',
                items: [{
                        id: uuid(),
                        name: "Spaghetti",
                        isEdit: false,
                        count: 1,
                        unit: "pkg"
                    }, {
                        id: uuid(),
                        name: "Vermicelli",
                        isEdit: true,
                        count: 0.5,
                        unit: "pkg"

                    }, {
                        id: uuid(),
                        name: "Strozzapreti",
                        isEdit: false,
                        count: 1.5,
                        unit: "pkg"

                    },
                    {
                        id: uuid(),
                        name: "Fettuccine",
                        isEdit: true,
                        count: 2,
                        unit: "pkg"
                    },
                ],
            }, {
                id: uuid(),
                name: "Kitchen Repair",
                type:'regularList',
                items: [{
                        id: uuid(),
                        name: "Oven",
                        isEdit: true,
                        count: 2,
                        unit: "pkg"
                    }, {
                        id: uuid(),
                        name: "Cooler",
                        isEdit: false,
                        count: 1,
                        unit: "pkg"

                    }, {
                        id: uuid(),
                        name: "Microwave",
                        isEdit: true,
                        count: 1,
                        unit: "pkg"

                    },
                    {
                        id: uuid(),
                        name: "Mixer",
                        isEdit: false,
                        count: 2,
                        unit: "pkg"
                    },
                    {
                        id: uuid(),
                        name: "Toaster",
                        isEdit: true,
                        count: 1,
                        unit: "pkg"
                    },
                ],
            },
        ]
    }
}



export function dataReducer(state = initialState, { type, payload }) {
    switch (type) {
        case ADD_USER_DETAL:
            {
                return {
                    ...state,
                    data: {
                        ...state.data,
                        username: payload.username,
                        imgUrl: payload.imgUrl
                    }
                }
            };
        case CREATE_LIST:
            {
                return {
                    ...state,
                    data: {
                        ...state.data,
                        [payload.type]: [
                            ...state.data[payload.type],
                            {
                                id: payload.id,
                                isCompleted: payload.isCompleted,
                                name: payload.name,
                                details: payload.details,
                                type: payload.type
                            }
                        ]
                    }
                }
            };
        case RESET_items:
            {
                return {
                    ...state,
                    data: {
                        ...state.data,
                        [payload.type]: state.data[payload.type].map(list => {

                            if (list.id === payload.listID) {
                                return {
                                    ...list,
                                    details: list.details.map(item => {
                                        if (item.isBought) {
                                            return {
                                                ...item,
                                                isBought: false
                                            }
                                        }
                                        return item
                                    })
                                }
                            }
                            return list
                        })
                    }

                }


            }
        case TOGGLE_items:
            {
                return {
                    ...state,
                    data: {
                        ...state.data,
                        [payload.type]: state.data[payload.type].map(list => {
                            if (list.id === payload.listID) {
                                return {
                                    ...list,
                                    details: list.details.map(item => {
                                        if (item.id === payload.itemID) {
                                            return {
                                                ...item,
                                                isBought: !item.isBought
                                            }
                                        }
                                        return item
                                    })
                                }
                            }
                            return list
                        })
                    }
                }
            }
        case DELETE_LIST:
            {
                return {
                    ...state,
                    data: {
                        ...state.data,
                        [payload.type]: state.data[payload.type].filter(list => list.id !== payload.listID)
                    }
                }
            }
        case UPDATE_ITEM:
            {
                return {
                    ...state,
                    data: {
                        ...state.data,
                        [payload.type]: state.data[payload.type].map(list => {
                            if (list.id === payload.listID) {
                                return {
                                    ...list,
                                    details: payload.details,
                                    type: payload.type
                                }
                            }
                            return list
                        })
                    }
                }
            }
        case SET_ALL_DATA:
            {
                return {
                    ...state,
                    data: payload.data,
                    type: payload.type
                }
            }
        default:
            return state;
    }
}
// ACTION CREATORS

export const addUserDetal = payload => ({
    type: ADD_USER_DETAL,
    payload
})
export const createList = payload => ({
    type: CREATE_LIST,
    payload
})
export const resetitems = payload => ({
    type: RESET_items,
    payload
})

export const toggleitem = payload => ({
    type: TOGGLE_items,
    payload
})



export const deleteList = payload => ({
    type: DELETE_LIST,
    payload
})

export const updateItem = payload => ({
    type: UPDATE_ITEM,
    payload
})

export const setAllData = payload => ({
    type: SET_ALL_DATA,
    payload
})


function createID() {
    return `${Date.now()}${Math.random()}`;
}