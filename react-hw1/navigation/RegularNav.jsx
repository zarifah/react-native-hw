import React from 'react';
import { createStackNavigator } from '@react-navigation/stack'

import { Regular, OneList, EditItem } from '../screens';

const { Navigator, Screen } = createStackNavigator();

export const RegularNav = () => {
  return (
      <Navigator headerMode="none">
          <Screen name="Regular Navigation" component={Regular} />
          <Screen name="One List" component={OneList} />
          <Screen name="Edit Item" component={EditItem} />


      </Navigator>
  )
}