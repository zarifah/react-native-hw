import React from 'react';
import { createStackNavigator } from '@react-navigation/stack'

import { OneTime, EditItem, OneList } from '../screens';

const { Navigator, Screen } = createStackNavigator();

export const OneTimeNav = () => {
  return (
      <Navigator headerMode="none">
          <Screen name="One Time Navigation" component={OneTime} />
          <Screen name="One List" component={OneList} />
          <Screen name="Edit Item" component={EditItem} />


      </Navigator>
  )
}