import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import {createDrawerNavigator } from "@react-navigation/drawer";
import {Drawer} from "../commons/Drawer"
import {
  AddNew,
  OneTime,
  Regular,
  UserSettings,
} from "../screens";
import {OneTimeNav} from "./OneTimeNav";
import {RegularNav} from "./RegularNav"

const { Navigator, Screen } =createDrawerNavigator();

export const RootNav = () => (
  <NavigationContainer>
    <Navigator headerMode="float" drawerContent={(props) => (<Drawer {...props}/>)} initialRouteName='User Settings'>
      <Screen name="Add New List"  component={AddNew} />
      <Screen name="One Time Lists" component={OneTimeNav} />
      <Screen name="Regular Lists" component={RegularNav} />
      <Screen name="User Settings" component={UserSettings} />
    </Navigator>
  </NavigationContainer>
);